<?php
/*
Plugin Name: Wordpress Post Archiver
Plugin URI:
Description: Archives posts and post meta data to local file system
Version: 1.0
Author: Anton Orlov
Author URI:
*/

require(dirname(__FILE__) . '/config.php');
require(PA_PLUGIN_PATH . '/functions/base.php');
require(PA_PLUGIN_PATH . '/functions/file.php');
spl_autoload_register('paAutoload');



    function paCron()
    {
        global $pa_tasks;
        echo "Processing PA cron jobs\n";
        foreach ($pa_tasks as $task_name => $task_class) {
            $current = new $task_class;
            $current->run();
        }
    }

    function paInstall()
    {
        global $wpdb;
        add_option('pa_publish_period', 90, 'Process posts older than period, days');
        add_option('pa_views', 2, 'Process posts with equal or less views');
        add_option('pa_posts_per_day', 100, 'Pack/Unpack posts per day');
        add_option('pa_unpack_total', 500, 'Unpack total posts');
        wp_schedule_event(time(), 'pa_interval', 'pa_cron_hook');

        $wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}pa_posts` (
            `post_id` int(11) NOT NULL,
            `done` tinyint(4) NOT NULL DEFAULT '0',
            PRIMARY KEY (`post_id`),
            KEY `done` (`done`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;");

        $wpdb->query("CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}pa_tasks` (
            `task_name` varchar(32) NOT NULL,
            `task_data` text NOT NULL,
            UNIQUE KEY `task_name` (`task_name`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
    }

    function paUnInstall()
    {
        delete_option('pa_publish_period');
        delete_option('pa_views');
        delete_option('pa_posts_per_day');
        delete_option('pa_unpack_total');
        wp_clear_scheduled_hook('pa_cron_hook');
    }

    function paCronSchedules($param)
    {
        $param['pa_interval'] = array (
                'interval' => round (PA_DAY_SECONDS / PA_CRON_RUNS), // seconds
                'display' => __('PA cron interval')
        );
        return $param;
    }

    function adminPaOptions()
    {
        $model = Model::getInstance();
        global $pa_tasks;
        foreach ($pa_tasks as $task_name => $task_class) {
            $current = $model->getTask($task_name);
            if (isset($current)) {
                $tasks[$task_name] = $current;
            }
        }
        include(PA_PLUGIN_PATH . '/views/header.php');
        if ($_REQUEST['submit']) {
            updatePaOptions();
        }
        include(PA_PLUGIN_PATH . '/views/main.php');
    }

    function updatePaOptions()
    {
        $ok = false;

        if (isset($_REQUEST['pa_publish_period'])) {
            update_option('pa_publish_period', (int)$_REQUEST['pa_publish_period']);
            $ok = true;
        }

        if (isset($_REQUEST['pa_views'])) {
            update_option('pa_views', (int)$_REQUEST['pa_views']);
            $ok = true;
        }

        if (isset($_REQUEST['pa_posts_per_day'])) {
            update_option('pa_posts_per_day', (int)$_REQUEST['pa_posts_per_day']);
            $ok = true;
        }

        if (isset($_REQUEST['pa_unpack_total'])) {
            update_option('pa_unpack_total', (int)$_REQUEST['pa_unpack_total']);
            $ok = true;
        }

        if ($ok) {
            include(PA_PLUGIN_PATH . '/views/ok.php');
        } else {
            include(PA_PLUGIN_PATH . '/views/error.php');
        }
    }

    function paMenu () {
        add_options_page (
            'Post archiver',    // page title
            'Post archiver',    // sub-menu title
            'manage_options',   // access/capability
            __FILE__,           // file
            'adminPaOptions'    // function
        );
    }

    add_filter('cron_schedules', 'paCronSchedules');
    add_action('pa_cron_hook', 'paCron');
    add_action('admin_menu', 'paMenu');
    register_activation_hook (__FILE__, 'paInstall');
    register_deactivation_hook (__FILE__, 'paUnInstall');
?>
