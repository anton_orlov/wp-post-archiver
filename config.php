<?php

define('PA_PLUGIN_PATH', dirname(__FILE__));
define('PA_TMP_DIR', PA_PLUGIN_PATH . '/var/');
define('PA_ARCHIVE_DIR', PA_PLUGIN_PATH . '/archives/');
define('PA_CLASSES_DIR', PA_PLUGIN_PATH . '/classes/');
define('PA_CRON_RUNS', 24);
define('PA_DAY_SECONDS', 86400);
define('PA_PACK_FILE_LIMIT', 4194304);
$pa_tasks = array(
    'post_pack'     => 'TaskPack',
    'post_unpack'   => 'TaskUnPack',
);
?>
