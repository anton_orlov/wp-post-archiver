<?php

$pa_publish_period = get_option('pa_publish_period');
$pa_views = get_option('pa_views');
$pa_posts_per_day = get_option('pa_posts_per_day');
$pa_unpack_total = get_option('pa_unpack_total');

$tPostPackStartDisabled = array(
    'new',
    'working',
);

$tPostUnpackStartDisabled = array(
    'new',
    'working',
);

$task_status = array (
    'new'       => 'new',
    'working'   => 'working',
    'stopped'   => 'stopped',
    'done'      => 'done',
);

$ajax_url = 'http://' . $_SERVER['HTTP_HOST'] . '/wp-content/plugins/post-archiver/ajax.php';

?>
<form method="post">
    <table border="0" cellspacing="3px">
        <tr>
            <td><label for="pa_publish_period">Process posts older than period, days:</label></td>
            <td><input type="text" size="10" name="pa_publish_period" value="<?php echo $pa_publish_period; ?>"></td>
        </tr>
        <tr>
            <td><label for="pa_views">Process posts with equal or less views:</label></td>
            <td><input type="text" size="10" name="pa_views" value="<?php echo $pa_views; ?>"></td>
        </tr>
        <tr>
            <td><label for="pa_posts_per_day">Pack/Unpack posts per day:</label></td>
            <td><input type="text" size="10" name="pa_posts_per_day" value="<?php echo $pa_posts_per_day; ?>"></td>
        </tr>
        <tr>
            <td><label for="pa_unpack_total">Unpack total posts:</label></td>
            <td><input type="text" size="10" name="pa_unpack_total" value="<?php echo $pa_unpack_total; ?>"></td>
        </tr>
    </table>
    <br />
    <input type="submit" class="button-primary" name="submit" value="Save">
</form>
<h3>Tasks</h3>
<table cellspacing="7px">
<tr>
    <td>
        Current time: <?php echo date('H:i d.m.y'); ?>
    </td>
</tr>
<?php

$tPostPackStatus = @$tasks['post_pack']['state'];
$tPostUnpackStatus = @$tasks['post_unpack']['state'];

?>
<tr>
    <td style="font-weight: bold">Post pack:</td>
    <td>
        <input class="start_task" data-name="post pack" <?php if ( in_array($tPostPackStatus, $tPostPackStartDisabled) || in_array($tPostUnpackStatus, $tPostUnpackStartDisabled) ) { echo ' disabled="disabled"'; } ?> type="button" name="post_pack" value="Start" /> &nbsp;
        <input class="stop_task" data-name="post pack" <?php if ( $tPostPackStatus == 'done' || $tPostPackStatus == 'stopped') { echo ' disabled="disabled"'; } ?> type="button" name="post_pack" value="Stop" />
    </td>
    <td>Status:
    <?php
        if (is_array(@$tasks['post_pack'])) {
            echo $task_status[$tPostPackStatus];
            if ($tPostPackStatus != 'new') {
                echo ", start time: " . $tasks['post_pack']['time_start'] . ", options: " . $tasks['post_pack']['task_options']. ", total: " . $tasks['post_pack']['total_todo'] . ", done: " . $tasks['post_pack']['total_done'] . ", done today: " . $tasks['post_pack']['total_done'] . "&nbsp;";
            }
        } else {
            echo "n/a";
        }
    ?>
    </td>
<tr>

<tr>
    <td style="font-weight: bold">Post unpack:</td>
    <td>
        <input class="start_task" data-name="post unpack" <?php if ( in_array($tPostPackStatus, $tPostPackStartDisabled) || in_array($tPostUnpackStatus, $tPostUnpackStartDisabled) ) { echo ' disabled="disabled"'; } ?> type="button" name="post_unpack" value="Start" /> &nbsp;
        <input class="stop_task" data-name="post unpack" <?php if ( $tPostUnpackStatus == 'done' || $tPostUnpackStatus == 'stopped') { echo ' disabled="disabled"'; } ?> type="button" name="post_unpack" value="Stop" />
    </td>
    <td>Status:
    <?php
        if (is_array(@$tasks['post_unpack'])) {
            echo $task_status[$tPostUnpackStatus];
            if ($tPostUnpackStatus != 'new') {
                echo ", start time: " . $tasks['post_unpack']['time_start'] . ", total: " . $tasks['post_unpack']['total_todo'] . ", done: " . $tasks['post_unpack']['total_done'] . ", done today: " . $tasks['post_unpack']['total_done'] . "&nbsp;";
            }
        } else {
            echo "n/a";
        }
    ?>
    </td>
<tr>

</table>
</div>
<script type="text/javascript">

jQuery(document).ready(function() {
    var ajaxUrl = '<?php echo $ajax_url; ?>';
    jQuery(".start_task").click(function() {
        var tName = jQuery(this).attr('name');
        var tText = jQuery(this).attr('data-name');
        if (tName == 'post_pack') {
            jQuery.ajax({
                type: "POST",
                async: false,
                url: ajaxUrl + "?action=CountPosts",
                    success: function(html) {
                        if (isNumber(html)) {
                            posts = html;
                        } else {
                            alert("Some Error Occurred. Response: " + html);
                            return false;
                        }
                    }
            });
            tText = tText + '. ' + posts + " posts will be packed";
        }
        if (confirm('Are you sure you want to run ' + tText + '?')) {
            jQuery.ajax({
                    type: "POST",
                    url: ajaxUrl + "?action=AddTask",
                    data: "task_name=" + tName,
                    success: function(html) {
                        if (html != '1') {
                            alert("Some Error Occurred. Response: " + html);
                            return false;
                        }
                    }
             });
        }
        return true;
    });
    jQuery(".stop_task").click(function() {
        var tName = jQuery(this).attr('name');
        var tText = jQuery(this).attr('data-name');
        if (confirm('Are you sure you want to stop ' + tText + '?')) {
            jQuery.ajax({
                    type: "POST",
                    url: ajaxUrl + "?action=StopTask",
                    data: "task_name=" + tName,
                    success: function(html) {
                        if (html != '1') {
                            alert("Some Error Occurred. Response: " + html);
                            return false;
                        }
                    }
            });
        }
        return true;
    });
function isNumber(o) {
    return !isNaN(o-0);
}
});
</script>
