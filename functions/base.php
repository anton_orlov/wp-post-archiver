<?php

function paAutoload($class_name)
{
    $file = PA_CLASSES_DIR . $class_name . '.php';
    if (file_exists($file)) {
        require_once(PA_CLASSES_DIR . $class_name . '.php');
    }
}

?>
