<?php

function gzip_file($infile, $outfile)
{
    $content = file_get_contents($infile);
    echo "Writing $outfile\n";
    $gz = gzopen($outfile, 'w9');
    gzwrite($gz, $content);
    gzclose($gz);
}

function ungzip_file($infile, $outfile)
{
    echo "Extracting {$infile} to {$outfile}...\n";
    $lines = gzfile($infile);
    $result = file_put_contents($outfile, implode("", $lines));
    return $result;
}

function extract_random_archive()
{
    $result = '';
    $files = glob(PA_ARCHIVE_DIR . 'posts_*');
    shuffle($files);
    if (!isset($files[0]))
        return $result;
    $g_path = $files[0];
    $t_filename = pathinfo($g_path, PATHINFO_FILENAME);
    $t_path = PA_TMP_DIR . $t_filename;
    if (ungzip_file($g_path, $t_path)) {
        $result = $t_path;
        echo "Deleting {$g_path}...\n";
        unlink($g_path);
    }
    return $result;
}

?>
