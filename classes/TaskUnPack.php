<?php

class TaskUnPack extends Task
{
    protected $name = 'post_unpack';
    protected $sequnce = array(
        'new',
        'working',
        'done',
    );
    protected $template = array(
        'state'         => 'new',
        'total_todo'    => 0,
        'total_done'    => 0,
        'done_today'    => 0,
        'time_start'    => '',
    );
    private $stateFile = 'unpack_state';

    protected function stateNew()
    {
        echo "Processing " . __METHOD__ . "\n";
        $this->data['day'] = date("d", time());
        $this->data['total_todo'] = get_option('pa_unpack_total');
        $this->data['time_start'] = date('H:i d.m.y');
        $this->nextState();
        $this->save();
    }

    protected function stateWorking()
    {
        echo "Processing " . __METHOD__ . "\n";
        $day = date("d", time());
        if ($day != $this->data['day']) {
            $this->data['done_today'] = 0;
            $this->data['day'] = $day;
            $this->save();
        }

        if (!isset($this->data['current_file']) || $this->data['current_file'] == '') {
            if ( ($oldState = $this->getFileState()) !== false) {
                $this->data['current_file'] = $oldState['file'];
                $this->data['start_line'] = $oldState['line'];
            } else {
                $this->data['current_file'] = extract_random_archive();
                $this->data['start_line'] = 0;
            }
            $this->save();
        }

        $state_done = false;
        $one_run = 3600 * 24 / PA_CRON_RUNS;
        $today_seconds = time() - mktime (0, 0, 0);
        $current_run = round($today_seconds/$one_run);
        $runs_left = PA_CRON_RUNS - $current_run;
        $posts_todo = ceil( (get_option('pa_posts_per_day') - $this->data['done_today']) / $runs_left);

        if ($this->data['current_file'] != '') {
            $handle = fopen($this->data['current_file'], "r");
            $current_line = -1;
            $insert_data = array();
            $lines_done = 0;
            while ( (($buffer = fgets($handle)) !== false) &&
                $lines_done < $posts_todo &&
                $this->data['total_todo'] > $this->data['total_done']) {

                $current_line++;
                if ($this->data['start_line'] > $current_line) {
                    continue;
                }
                $this->data['start_line']++;
                $post_array = json_decode(trim($buffer), true);

                if (is_array($post_array)) {
                    $post_data = array(
                        'post_title'        => $post_array['post_title'],
                        'post_content'      => $post_array['post_content'],
                        'post_status'       => 'publish',
                        'post_author'       => $post_array['post_author'],
                        'post_category'     => array($post_array['term_id']),
                        'comment_status'    => 'closed',
                        'ping_status'       => 'closed',
                    );

                    $exp_id = wp_insert_post($post_data);

                    if ($exp_id > 0) {
                        if (isset($post_array['meta'])) {
                            foreach ($post_array['meta'] as $meta_key => $meta_value) {
                                add_post_meta($exp_id, $meta_key, $meta_value);
                            }
                        }
                        $lines_done++;
                        $this->data['total_done']++;
                        $this->data['done_today']++;
                    }
                }

                $this->saveFileState($this->data['current_file'], $this->data['start_line']);
                $this->save();
            }

            fclose($handle);

            if ($buffer === false) {
                unlink($this->data['current_file']);
                $this->data['current_file'] = extract_random_archive();
                $this->data['start_line'] = 0;
            }

            if ($this->data['total_done'] >= $this->data['total_todo']) {
                $this->nextState();
            }
        } else {
            echo "No archive files found\n";
            $this->nextState();
        }
        $this->saveFileState($this->data['current_file'], $this->data['start_line']);
        $this->save();
    }

    protected function stateDone()
    {
        echo "Processing " . __METHOD__ . "\n";
        return true;
    }

    private function saveFileState($file, $line)
    {
        $data = array(
            'file' => $file,
            'line' => $line,
        );
        return file_put_contents(PA_TMP_DIR . $this->stateFile, serialize($data));
    }

    private function getFileState()
    {
        $result = false;
        if (is_file(PA_TMP_DIR . $this->stateFile)) {
            $data = file_get_contents(PA_TMP_DIR . $this->stateFile);
            $result = unserialize($data);
        }
        return $result;
    }

}

?>
