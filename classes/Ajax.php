<?php

class Ajax {

    private $actions = array('AddTask', 'StopTask', 'CountPosts');

    public function __construct($request)
    {
        global $pa_tasks;
        $this->tasks = $pa_tasks;
        if (!in_array($request, $this->actions) OR !$this->isAuthed())
            $action = 'actionBad';
        else
            $action = 'action'.$request;
        $this->$action();
    }

    private function isAuthed()
    {
        return current_user_can('update_core');
    }

    private function actionAddTask()
    {
        $name = $_POST['task_name'];
        if (isset($_POST['args']) && is_array($_POST['args'])) {
            $args = $_POST['args'];
        } else {
            $args = array();
        }
        if (isset($this->tasks[$name])) {
            $classname = $this->tasks[$name];
            $task = new $classname;
            $r = $task->create($args);
        } else {
            $this->action_bad();
        }
       $this->render($r);
    }

    private function actionStopTask()
    {
        $name = $_POST['task_name'];
        if (isset($this->tasks[$name])) {
            $stop_file = PA_TMP_DIR . $name . '_stopped';
            $r = (int)file_put_contents($stop_file, "\n");
        } else {
            $this->action_bad();
        }
        $this->render($r);
    }

    private function actionCountPosts()
    {
        $publish_period = get_option('pa_publish_period');
        $views = get_option('pa_views');
        $model = Model::getInstance();
        $posts = $model->getPostCount($publish_period, $views);
        $this->render($posts);
    }

    private function actionBad()
    {
        $this->render('Bad request');
    }

    private function render($data)
    {
        echo $data;
        exit();
    }
}

?>
