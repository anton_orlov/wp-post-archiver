<?php

class TaskPack extends Task
{
    protected $name = 'post_pack';
    protected $sequnce = array(
        'new',
        'working',
        'done',
    );
    protected $template = array(
        'state'         => 'new',
        'total_todo'    => 0,
        'total_done'    => 0,
        'done_today'    => 0,
        'time_start'    => '',
        'task_options'  => '',
    );
    private $optimize_tables = array(
        'posts',
        'postmeta',
        'term_relationships',
    );

    protected function stateNew()
    {
        echo "Processing " . __METHOD__ . "\n";
        $publish_period = get_option('pa_publish_period');
        $views = get_option('pa_views');
        $this->data['total_todo'] = $this->model->fillTaskPost($publish_period, $views);
        $this->data['time_start'] = date('H:i d.m.y');
        $this->data['task_options'] = 'days: ' . $publish_period . ', views: ' . $views;
        $this->data['day'] = date("d", time());
        $this->nextState();
        $this->save();
    }

    protected function stateWorking()
    {
        echo "Processing " . __METHOD__ . "\n";
        $day = date("d", time());
        if ($day != $this->data['day']) {
            $this->data['done_today'] = 0;
            $this->data['day'] = $day;
            $this->save();
        }

        if ($this->data['current_file'] == '') {
            $this->data['current_file'] = 'posts_' . str_replace('.', '', microtime(true)) . '.txt';
            $this->save();
        }

        $state_done = false;
        $current_file_path = PA_TMP_DIR . $this->data['current_file'];
        $current_gfile_path = PA_ARCHIVE_DIR . $this->data['current_file'] . '.gz';
        $one_run = 3600 * 24 / PA_CRON_RUNS;
        $today_seconds = time() - mktime (0, 0, 0);
        $current_run = round($today_seconds/$one_run);
        $runs_left = PA_CRON_RUNS - $current_run;
        $posts_todo = ceil( (get_option('pa_posts_per_day') - $this->data['done_today']) / $runs_left);

        if ($posts_todo > 0 && $this->data['total_done'] < $this->data['total_todo'] ) {
            $post_ids = $this->model->getUndonePostIds($posts_todo);
            if (count($post_ids) > 0) {
                foreach ($post_ids as $post_id) {
                    $post_data = $this->model->getPostData($post_id);
                    $json_encoded = json_encode($post_data);
                    file_put_contents($current_file_path, $json_encoded."\n", FILE_APPEND);
                    $r1 = $this->model->setPostDone($post_id);
                    $r2 = wp_delete_post($post_id, true);
                    $this->data['total_done']++;
                    $this->data['done_today']++;
                    $this->save();
                }
            }
        }

        if ($this->data['total_done'] >= $this->data['total_todo']) {
            $this->nextState();
            $state_done = true;
        }
        clearstatcache();
        if ( file_exists($current_file_path) && ( filesize($current_file_path) > PA_PACK_FILE_LIMIT || $state_done) ) {
            gzip_file($current_file_path, $current_gfile_path);
            unlink($current_file_path);
            $this->data['current_file'] = 'posts_' . str_replace('.', '', microtime(true)) . '.txt';
        }
        $this->save();
        if ($state_done) {
            $this->model->optimizeTables($this->optimize_tables);
        }
    }

    protected function stateDone()
    {
        echo "Processing " . __METHOD__ . "\n";
        return true;
    }
}

?>
