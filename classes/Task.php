<?php

abstract class Task
{
    protected $name = '';
    protected $sequnce = array();
    protected $template = array();
    protected $model = null;
    protected $data = array();
    private $stop_file = '';
    private $state_key = 0;

    public function __construct()
    {
        $this->model = Model::getInstance();
        $this->stop_file = PA_TMP_DIR . $this->name . '_stopped';
    }

    public function run()
    {
        $this->load();
        $current_state = $this->getState();
        if (isset($current_state)) {
            foreach ($this->sequnce as $this->state_key => $state) {
                $method = 'state' . ucfirst($state);
                if ($current_state == $state && method_exists($this, $method)) {
                    $this->$method();
                }
            }
        }
    }

    abstract protected function stateNew();

    abstract protected function stateDone();

    protected function nextState()
    {
        $next_key = $this->state_key + 1;
        if (isset($this->sequnce[$next_key])) {
            $this->data['state'] = $this->sequnce[$next_key];
        }
    }

    private function getState()
    {
        $result = null;
        if (isset($this->data['state']))
            $result = $this->data['state'];
        return $result;
    }

    public function create($args = array())
    {
        if (file_exists($this->stop_file)) {
            unlink($this->stop_file);
        }
        if (count($args) > 0) {
            foreach ($args as $key => $value) {
                $this->template[$key] = $value;
            }
        }
        return $this->model->addTask($this->name, $this->template);
    }

    public function load()
    {
        $this->data = $this->model->getTask($this->name);
    }

    public function save()
    {
        if (file_exists($this->stop_file)) {
            $this->data['state'] = 'stopped';
        }
        $result = $this->model->updateTask($this->name, $this->data);
        if (file_exists($this->stop_file)) {
            exit("Exiting... Task {$this->name} recieved stop signal\n");
        }
        return $result;
    }
}

?>
