<?php

class Model
{
    private $db = null;
    private static $instance = null;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            $className = __CLASS__;
         self::$instance = new $className;
        }
        return self::$instance;
    }

    public function __construct ()
    {
        global $wpdb;
        $this->db = $wpdb;
    }

    public function getPostCount($publishPeriod, $views)
    {
        $result = 0;
        $startDate = date('Y-m-d H:i:s', time() - $publishPeriod * 24 * 3600);
        $query = $this->db->prepare("SELECT COUNT(*) FROM `{$this->db->prefix}posts` as t1
            INNER JOIN `{$this->db->prefix}postmeta` as t2
            ON `t1`.`ID` = `t2`.`post_id`
            WHERE `t1`.`post_type` =  'post' AND `t1`.`post_status` = 'publish' AND
            `t1`.`post_date` < '{$startDate}' AND `t2`.`meta_key` = 'views' AND `t2`.`meta_value` <= {$views}");
        $result = $this->db->get_var($query);
        return $result;
    }

    public function fillTaskPost($publishPeriod, $views)
    {
        $result = 0;
        $startDate = date('Y-m-d H:i:s', time() - $publishPeriod * 24 * 3600);
        $query1 = $this->db->prepare("TRUNCATE TABLE `{$this->db->prefix}pa_posts`");
        $query2 = $this->db->prepare("INSERT INTO `{$this->db->prefix}pa_posts` (`post_id`)
            SELECT `t1`.`ID` as `post_id` FROM `{$this->db->prefix}posts` as t1
            INNER JOIN `{$this->db->prefix}postmeta` as t2
            ON `t1`.`ID` = `t2`.`post_id`
            WHERE `t1`.`post_type` =  'post' AND `t1`.`post_status` = 'publish' AND
            `t1`.`post_date` < '{$startDate}' AND `t2`.`meta_key` = 'views' AND `t2`.`meta_value` <= {$views}");
        $this->db->query($query1);
        $result = $this->db->query($query2);
        return $result;
    }

    public function getUndonePostIds($limit = 10)
    {
        $result = array();
        $query = $this->db->prepare("SELECT `post_id` FROM `{$this->db->prefix}pa_posts` WHERE `done` = 0 LIMIT 0,{$limit}");
        $result = $this->db->get_col($query);
        return $result;
    }

    public function setPostDone($postId)
    {
        $result = false;
        $query = $this->db->prepare("UPDATE `{$this->db->prefix}pa_posts` SET `done` = 1 WHERE `post_id` = $postId");
        $result = $this->db->query($query);
        return $result;
    }

    public function getPostData($post_id)
    {
        $result = array();
        $query = $this->db->prepare("SELECT `t1`.`ID`, `t1`.`post_author`, `t1`.`post_content`, `t1`.`post_title`, `t2`.`meta_key`, `t2`.`meta_value`, `t4`.`term_id`
            FROM `{$this->db->prefix}posts` as `t1`
            INNER JOIN `{$this->db->prefix}postmeta` as `t2`
            ON `t2`.`post_id` = `t1`.`ID`
            INNER JOIN `{$this->db->prefix}term_relationships` as `t3`
            ON `t3`.`object_id` = `t1`.`ID`
            INNER JOIN `{$this->db->prefix}term_taxonomy` as `t4`
            ON `t4`.`term_taxonomy_id` = `t3`.`term_taxonomy_id`
            WHERE `t1`.`ID` = {$post_id}");
        $r = $this->db->get_results($query, ARRAY_A);
        foreach ($r as $key => $row) {
            if ($key == 0) {
                $result = array(
                    'ID'            => $row['ID'],
                    'post_author'   => $row['post_author'],
                    'post_content'  => $row['post_content'],
                    'post_title'    => $row['post_title'],
                    'term_id'       => $row['term_id'],
                );
            }
            $result['meta'][$row['meta_key']] = $row['meta_value'];
        }
        return $result;
    }

    public function optimizeTables($tables)
    {
        if (count($tables) > 0) {
            foreach ($tables as $table) {
                $table_name = $this->db->prefix . $table;
                echo "Optimizing table $table_name\n";
                $this->db->query("OPTIMIZE TABLE `{$table_name}`");
            }
        }
    }

    public function addTask ($task_name, $data)
    {
        $result = 0;
        $task_data = serialize($data);
        $query = $this->db->prepare("INSERT INTO `{$this->db->prefix}pa_tasks` (`task_name`, `task_data`) VALUES (%s, %s)
                    ON DUPLICATE KEY UPDATE `task_data` = %s",
                    $task_name,
                    $task_data,
                    $task_data
        );
        $result = (int)(bool)$this->db->query($query);
        return $result;
    }

    public function getTask($task_name)
    {
        $result = null;
        $query = $this->db->prepare("SELECT `task_data` FROM `{$this->db->prefix}pa_tasks` WHERE `task_name` = %s",
            $task_name
        );
        $result = $this->db->get_var($query);
        if (isset($result)) {
            $result = unserialize($result);
        }
        return $result;
    }

    public function updateTask($task_name, $data)
    {
        $result = 0;
        $task_data = serialize($data);
        $query = $this->db->prepare("UPDATE `{$this->db->prefix}pa_tasks` SET `task_data` = %s WHERE `task_name` = %s",
                $task_data,
                $task_name
        );
        $result = $this->db->query($query);
        return $result;
    }
}

?>
