Install:

1. Unpack plugin files to plugins/post-archiver/ directory
2. Make sure that archives/ and var/ directories have 0777 rights
3. Add the following code to wp-config.php
	define('DISABLE_WP_CRON', true);
4. Setup new cron job:
	*/10	*	*	*	*	/usr/bin/php /path_to_document_root/wp-cron.php >/dev/null 2>&1
5. If needed edit config.php
6. Activate plugin
7. Setup necessary options on `Post Archiver` options page
8. Run jobs
9. Archived files will be stored in archives/ folder
10. Enjoy

config.php constants:

PA_CRON_RUNS - number of cron runs per day
PA_PACK_FILE_LIMIT - maximum size of file with posts to pack
